""" Distributed under GNU Affero GPL license.
    
    Versione 1.1.2 White Rabbit 28/08/2019.

    Copyright (C) 2019  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    Text To Txt Bot is a Telegram Bot that transform messages in .txt files.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""


#-- BOT'S START ---

from CONFIG import *
import to_channel
import staff_manager
import database_manager

def on_chat_message(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)

    with open(database_staff,'rb') as file:
        #The staff composition has been saved on a specific database, so that is easy to modify it for the user
        #There the bot extract the list that contains all the staff's IDs
        staff = pickle.load(file)
        
        if main_admin not in staff:
            #Makes sure the main_admin is considered part of the staff, so you have not to manually change the list if change the main_admin
            staff.append(main_admin)
    
    if chat_type == 'private' and chat_id in staff:

        if content_type == 'text':
            
            command = msg['text']

            if 'forward_from' in list(msg.keys()) and chat_id == main_admin and msg['forward_from']['id'] != main_admin:
                #The message has been forwarded from a user; text message forwarded are used to add or remove users from the staff

                user_id = msg['forward_from']['id']
                
                if user_id not in staff:
                    #This is used to add a user to the staff
                    
                    keyboard = InlineKeyboardMarkup(
                        inline_keyboard=(
                            [[InlineKeyboardButton(
                                text = '➕ Add to the staff',
                                callback_data = 'add_staff '+str(user_id))
                              ]]
                            )
                        )


                else:
                    #This is used to remove a user from the staff

                    keyboard = InlineKeyboardMarkup(
                        inline_keyboard=(
                            [[InlineKeyboardButton(
                                text = '➖ Remove from the staff',
                                callback_data = 'remove_staff '+str(user_id))
                              ]]
                            +
                            [[InlineKeyboardButton(
                                text = '✖️ Remove anything from',
                                callback_data = 'remove_all '+str(user_id))
                              ]]
                            )
                        )

                message = "I have identified this user. What do you want to do with it?"

                bot.sendMessage(
                    chat_id = chat_id,
                    text = message,
                    reply_markup = keyboard,
                    reply_to_message_id = msg['message_id']
                    )
 
                    
                

            elif command == "/start" or command == "/help" or command == "/info":
                
                message = "Hello, I'm here to help you in your work.\n\nJust send me a media, I'll add it to the row and send it automatically.\nIf you want to remove a media from the row, resend it :-)"
                
                keyboard = InlineKeyboardMarkup(
                    inline_keyboard=(
                        [[InlineKeyboardButton(
                            text = 'Source Code',
                            url = source_link)
                          ]]
                        )
                    )

                bot.sendMessage(
                    chat_id = chat_id,
                    text = message,
                    reply_markup = keyboard,
                    reply_to_message_id = msg['message_id']
                    )


            elif command.split()[0] == "/start":
                #It's probably a deep link
                message = " "

            elif command == "/stat":

                already_sent = 0 #Count the media sent by the bot
                to_send = 0 #Count the media marked as '0', to be sent
                your_media = 0 #Count the media sent by the user

                archive = json.load(open(database))
                
                media = list(archive.keys())
                
                for file_id in media:
                    
                    if archive[file_id]['status'] == 1:
                        already_sent += 1

                    elif archive[file_id]['status'] == 0:
                        to_send += 1

                    if archive[file_id]['author'] == chat_id:
                        your_media += 1

                    
                    
                message = "The bot has listed "+str(len(media))+" media."
                message += "\nOf this, "+str(already_sent)+" has been signed as 'already sent'."
                message += "\nThe bot has "+str(to_send)+" media listed in the row that will be sent."
                message += "\nIn total, you have sent "+str(your_media)+" media to the bot."

                bot.sendMessage(
                    chat_id = chat_id,
                    text = message,
                    reply_to_message_id = msg['message_id']
                    )

            elif command == "/staff" and chat_id == main_admin:
                message = "To add or remove a user from the staff, please *forward* to me one of his *textual* message."

                bot.sendMessage(
                    chat_id = chat_id,
                    text = message,
                    parse_mode = "Markdown",
                    reply_to_message_id = msg['message_id']
                    )

            elif command == "/old_media" and chat_id == main_admin:
                #This command, reserved to the main admin, is used to manage the old media (defined as media older than 21 days).
                #It get you to know how many old media there are and let you delete them or see them. 
                
                old_media = len(database_manager.old_media(21))
                message = "There are "+str(old_media)+" media that have been added three or more weeks ago. Do you want to delete them?"

                keyboard = InlineKeyboardMarkup(
                    inline_keyboard = (
                        [[InlineKeyboardButton(
                            text = '✅ Yes, delete them',
                            callback_data = 'delete_old_media'
                            )
                          ]]+
                        [[InlineKeyboardButton(
                            text = '\U0001F4F7 See them',
                            callback_data = 'show_old_media'
                            )
                          ]]
                        )
                    )
                        
                bot.sendMessage(
                    chat_id = chat_id,
                    text = message,
                    reply_markup = keyboard,
                    reply_to_message_id = msg['message_id']
                    )

                                        
        elif content_type == 'photo' or content_type == 'video' or content_type == 'document':
            
            archive = json.load(open(database))

            if content_type == 'photo':
                file_id = msg['photo'][0]['file_id']
                
            else: #Document and video
                file_id = msg[content_type]['file_id']

            if file_id in list(archive.keys()):
                #The admin has sent a media already added to the database, and this may mean he wants to remove the media

                #Status zero means that the media has been added to the row but not sent yet
                #Status one means that the media has been already sent to the channel
                #Status two means that the media has been removed from the row and not sent yet

                if archive[file_id]['status'] == 0:
                    #The bot removes the media from the row
                    
                    archive[file_id]['status'] = 2
                    
                    message = "This media has been removed from the row."
                    
                    bot.sendMessage(
                        chat_id = chat_id,
                        text = message,
                        reply_to_message_id = msg['message_id']
                        )

                    if chat_id != main_admin:
                        #This is used to notice the main admin about the change in the row
                        
                        message = "The user ["+bot.getChat(chat_id)['first_name']+"](tg://user?id="+str(chat_id)+") has removed a "+content_type+" from the row."
                        staff_manager.notice_main_admin(message,file_id,content_type)
                    

                elif archive[file_id]['status'] == 1:
                    #The bot sent a warn and ask for a confirmation: should the media be sent again?
                    #The callback data contains the file_id, that will identify the photo, and the code sa (send again)
                    
                    message = "This media has been already sent to the main channel. Do you want to send it again?"
                    
                    keyboard = InlineKeyboardMarkup(
                        inline_keyboard = (
                            [[InlineKeyboardButton(
                                text = '✅ Yes, I do',
                                callback_data = ('sa '+file_id)
                                )
                              ]]
                            )
                        )
                    
                    bot.sendMessage(
                        chat_id = chat_id,
                        text = message,
                        reply_markup = keyboard,
                        reply_to_message_id = msg['message_id']
                        )


                elif archive[file_id]['status'] == 2:
                    #This media was previously removed from the crow, so the bot add it there again
                    #To do so, the media will be treated as it was not in the database

                    del archive[file_id]
                
               
                
                
            if file_id not in list(archive.keys()):
                #There will add the media to the database
                
                archive[file_id] = {
                    'author' : chat_id,
                    'time' : msg['date'],
                    'status' : 0,
                    'type' : content_type
                    }
                
                message = "This media has been added to the row."

                bot.sendMessage(
                    chat_id = chat_id,
                    text = message,
                    reply_to_message_id = msg['message_id']
                    )

                if chat_id != main_admin:
                    #This is used to notice the main admin about the change in the row

                    message = "The user ["+bot.getChat(chat_id)['first_name']+"](tg://user?id="+str(chat_id)+") has added a new "+content_type+" to the row."
                    staff_manager.notice_main_admin(message,file_id,content_type)


            with open(database,'w') as outfile:
                #Update the database
                json.dump(archive,outfile,indent=4)
                    

def on_callback_query(msg):
    query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
    message_id = msg['message']['message_id']

    
    if query_data.split()[0] == 'sa':
        #The code sa (send again) is followed by the file_id which status in database has to be changed to zero, so that it will be added again the the row 

        file_id = query_data.split()[1]
        archive = json.load(open(database))
        archive[file_id]['status'] = 0

        with open(database,'w') as outfile:
            json.dump(archive,outfile,indent=4)
        

        bot.editMessageText(
            msg_identifier = (from_id, message_id),
            text = "This media has been added again to the row.",
            reply_markup = None
            )

        if from_id != main_admin:
            #This is used to notice the main admin about the change in the row

            content_type = archive[file_id]['type']
                        
            message = "The user ["+bot.getChat(chat_id)['first_name']+"](tg://user?id="+str(chat_id)+") has added again a "+content_type+" to the row."
            staff_manager.notice_main_admin(message,file_id,content_type)



    elif query_data == 'not_confirmed':
        #This command is used when the user does not confirm an action, so that the operation is cancelled

        message = "Operation cancelled."

        bot.editMessageText(
            msg_identifier = (from_id, message_id),
            text = message,
            reply_markup = None
            ) 

    elif query_data == "delete_old_media":
        #This command is used to delete media older than 21 days

        media = database_manager.delete_old_media(21)
        message = "Deleted "+str(media)+" media older than 21 days with success."

        bot.editMessageText(
            msg_identifier = (from_id, message_id),
            text = message,
            reply_markup = None
            )

    elif query_data == 'show_old_media':
        #Send to the main admin all the media older than 21 days

        media = database_manager.old_media(21)

        for file_id in media:

            bot.sendPhoto(
                chat_id = main_admin,
                photo = file_id
                )

    
    elif query_data.split()[0] == 'see':
        #When announcing a new media, the bot writes in the message text what kind of media it's (es. "a new photo was added")
        #The callback cointains the file_id, while the media type is extracted from the message text
        
        message_text = msg['text']
        file_id = query_data.split()[1]
        
        if 'photo' in message_text:

            bot.sendPhoto(
                chat_id = from_id,
                photo = file_id
                )
        elif 'document' in message_text:

            bot.sendDocument(
                chat_id = from_id,
                document = file_id
                )

        elif 'video' in message_text:

            bot.sendVideo(
                chat_id = from_id,
                video = file_id
                )

    elif query_data.split()[0] == 'remove_staff':
        #Remove a user from the staff. The callback is composed by the command and the user's ID.
        #Please notice that the ID has been received in str but has to be converted in int, as is usually rappresented
        staff_manager.remove_from_staff(int(query_data.split()[1]),message_id)

    elif query_data.split()[0] == 'add_staff':
        #Add a user to the staff. The callback is composed by the command and the user's ID.
        #Please notice that the ID has been received in str but has to be converted in int, as is usually rappresented
        staff_manager.add_to_staff(int(query_data.split()[1]),message_id)

    elif query_data.split()[0] == 'remove_all':
        #Ask a confirmation to remove all the media by a user. The callback is composed by the command and the user's ID.

        message = "Please notice that this action will remove from the row all the media added by this user. Do you want to continue?"

        keyboard = InlineKeyboardMarkup(
            inline_keyboard=(
                [[InlineKeyboardButton(
                    text = '✔️ Yes',
                    callback_data = 'remove_all_con '+str(query_data.split()[1]))
                  ]]
                +
                [[InlineKeyboardButton(
                    text = '✖️ No',
                    callback_data = 'not_confirmed')
                  ]]
                )
            )

        bot.editMessageText(
            msg_identifier = (from_id, message_id),
            text = message,
            reply_markup = keyboard
            )

    elif query_data.split()[0] == 'remove_all_con':
        #Remove all the media by a user. The callback is composed by the command and the user's ID.
        #Please notice that the ID has been received in str but has to be converted in int, as is usually rappresented
        staff_manager.remove_anything_from(int(query_data.split()[1]),message_id)

                                    
        

      

#-- BOT'S TAIL --

MessageLoop(bot, {'chat': on_chat_message,
       'callback_query': on_callback_query}).run_as_thread()
#Receive the messagges, and process them in different ways if they are keyboard or not

action = 0


while 1: #Keeps the bot alive and check to send the media

    if int(time.strftime('%H')) in hours and action == 0:
        #If it's one of the hours indicated to sent one media in the channel, send it
        
        while True:
            try:
                to_channel.send()
                break
            except:
                None
                
        action = time.strftime('%H')

    elif action != time.strftime('%H'):
        #This sistem will reset the value action to zero every time the hour change or, in other words, every hour 
        #As time.strtfime is an str, there is no possible error with the value 0 of action, that indicates an images can be sent

        action = 0

    time.sleep(5)

