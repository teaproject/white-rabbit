# White Rabbit

White Rabbit is a Telegram Bot designed for channels that works with media. 
In a simple and intuitive way, this bot let you manage media and send them automatically to the channel.
Supporting staff options, it protect your channel from damages.     

This bot has been developed in Python 3.6 using the [telepot](https://github.com/nickoala/telepot/) library. 

This is the perfect solution for you if you have a channel that is based on:
- Books and magazine;
- Wallpapers; 
- Photos of any sort; 
- Memes; 
- Any other kind of media.

This bot can be used for: 

*  Add and remove media from the list; 
*  Automatically send media into a channel; 
*  Manage your staff; 
*  Get information about the media quequed and delete the older ones; 
*  Automatically set a caption to the media posted.

It not only helps you manage your channel by posting in automatic, but also **add a layer of security to your channel**.
In fact, by using this bot you will no more have to add staffer as "channel admninistrators", letting them the possibility to cause several damage to the channel (such as, removing all members).
Instead, you will only have to add them in the bot's staff, and they will have no way to damage the channel. 

This bot is free software, feel free to modify it in the way you prefer. 