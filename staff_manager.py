""" This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2019 - Davide Leone """


from CONFIG import *

def notice_main_admin(message,file_id,content_type):

    """This function is used to notice the main admin when a media
    is added or removed from the row. The main admin will receive a messae
    with the information about the admin that added/removed the media, and
    will have a button thanks to he can see the media. The content_type is
    used to understand what functions has to be used to show the media.
    This information will be stored in form of see-v for videos, see-p for
    videos and see-d for documents."""

    from CONFIG import bot, channel, telepot, main_admin

    callback_text = "see "+file_id

    keyboard = InlineKeyboardMarkup(
        inline_keyboard = (
            [[InlineKeyboardButton(
                text = '\U0001F4F7 See it',
                callback_data = callback_test
                )
              ]]
            )
        )

    bot.sendMessage(
        chat_id = main_admin,
        text = message,
        parse_mode = 'Markdown',
        reply_markup = keyboard
        )


def getCaption():

    """This function is used to send create the caption for the photos.
    The caption is composed by the title of the channel, formatted with
    it's link. So, the bot try to get the link, otherwise generate a new
    one, then use HTML to create the caption."""

    from CONFIG import bot, channel, telepot
    
    chat = bot.getChat(channel)

    if 'invite_link' in list(chat.keys()):
        link = chat['invite_link']

    else:
        link = bot.exportChatInviteLink(channel)

    name = html.escape(chat['title'],True)
    #There is used the html escape to avoid any possible error

    caption = '<a href="'+link+'">'+name+'</a>'

    return caption


def add_to_staff(user,message_id):
    
    """This function is used to add a user to the staff."""

    from CONFIG import bot, telepot, main_admin, database_staff

    with open(database_staff,'rb') as file:
        #The staff composition has been saved on a specific database, so that is easy to modify it for the user
        #There the bot extract the list that contains all the staff's IDs
        staff = pickle.load(file)

    if user not in staff:

        staff.append(user)
    
        with open(database_staff,'wb') as outfile:
            pickle.dump(staff,outfile)

        message = "The user was correctly added to the staff."

        bot.editMessageText(
            msg_identifier = (main_admin, message_id),
            text = message,
            reply_markup = None
            )

    else:

        message = "The user is already part of the staff."

        bot.editMessageText(
            msg_identifier = (main_admin, message_id),
            text = message,
            reply_markup = None
            )
        

def remove_from_staff(user,message_id):

    """This function is used to remove a user from the staff."""

    from CONFIG import bot, telepot, main_admin, database_staff

    with open(database_staff,'rb') as file:
        #The staff composition has been saved on a specific database, so that is easy to modify it for the user
        #There the bot extract the list that contains all the staff's IDs
        staff = pickle.load(file)

    if user in staff:

        staff.remove(user)
    
        with open(database_staff,'wb') as outfile:
            pickle.dump(staff,outfile)

        message = "The user was correctly removed from the staff."

        bot.editMessageText(
            msg_identifier = (main_admin, message_id),
            text = message,
            reply_markup = None
            )

    else:

        message = "The user has not been found in the staff's composition list."

        bot.editMessageText(
            msg_identifier = (main_admin, message_id),
            text = message,
            reply_markup = None
            )

def remove_anything_from(user,message_id):

    """This function is used to remove from the row all the media by a user."""

    from CONFIG import bot, telepot, main_admin, database

    archive = json.load(open(database)) 

    deleted_count = 0

    for media in list(archive.keys()):

        if archive[media]['author'] == user and archive[media]['status'] == 0:

            archive[media]['status'] = 2
            deleted_count += 1
            

    with open(database,'w') as outfile:
        #Update the database
        json.dump(archive,outfile,indent=4)

    message = "I have removed from the row all the media by the user.\n\nIn total, I have removed "+str(deleted_count)+" media."

    bot.editMessageText(
        msg_identifier = (main_admin, message_id),
        text = message,
        reply_markup = None
        )

    
            
    
